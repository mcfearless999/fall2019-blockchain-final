
struct Player_entry:
   playerAddress: address
   playerStats: bytes32
   PC_value: wei_value

owner1: (address)
owner2: (address)
MAX_PC_NUM: constant(int128) = 4096
#current_PC_num: int128
players: map(address, Player_entry) 
cur_rand_num: bytes32
#cur_validation_hash: bytes32
MAX_RAND_NUM: constant(int128) = 1048576
DEFAULT_STATS: bytes[8] 
rand_counter: int128
waiting_player: address

@public
def __init__():
   self.owner1 = 0xA319E6EFad7fEe6046913fE7F3c1E48df14aDE6c
   self.owner2 = 0xBce85CFC3F79E8677bdE56C09Ce2BD62c9Ed442A
   self.DEFAULT_STATS = b'\x00\x00\x00\x00\x04\x01\x01\x01'
   self.cur_rand_num = block.prevhash
   self.rand_counter = 0

@public
@payable 
def gen_player():
   self.players[msg.sender] = Player_entry({playerAddress: msg.sender,playerStats:convert(self.DEFAULT_STATS,bytes32),PC_value: msg.value})

@private 
def gen_random() -> bytes32:
   if self.rand_counter == MAX_RAND_NUM:
      self.rand_counter = 0 
   for idx in range (MAX_RAND_NUM):
      self.cur_rand_num = keccak256(self.cur_rand_num)
      if idx > (MAX_RAND_NUM - self.rand_counter):
         break
   self.rand_counter += 1
   return self.cur_rand_num

